# GTFS Realtime

Read GTFS Realtime proto-buffer response in Ruby.


## Install

Downlad the latest gem release in [Downloads](https://bitbucket.org/enroute-mobi/gtfs-rt/downloads/)

```
curl -Lfs -o gtfs-rt-0.1.0.gem https://bitbucket.org/enroute-mobi/gtfs-rt/downloads/gtfs-rt-0.1.0.gem
gem install gtfs-rt-0.1.0.gem
```

## Usage

```
gtfs-rt-to-json https://ara-api.enroute.mobi/test/gtfs > output.json
TOKEN=secret gtfs-rt-to-json https://ara-api.enroute.mobi/test/gtfs > output.json

gtfs-rt-to-text https://ara-api.enroute.mobi/test/gtfs > output.txt
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## License

The gem is available as open source under the terms of the [Apache Licence 2](https://opensource.org/licenses/Apache-2.0).

## Support

Contact [enRoute team](mailto:contact@enroute.mobi) to know how to contribute to this project.
