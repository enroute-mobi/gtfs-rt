require 'spec_helper'

RSpec.describe GTFS::Realtime do
  it 'can read a simple GTFS-RT feed' do
    sample_feed = GTFS::Realtime::FeedMessage.parse(File.read("spec/fixtures/gtfs-rt-irigo.pb"))
    expect(sample_feed).to be_a(GTFS::Realtime::FeedMessage)
  end
end
