require 'protocol_buffers'

require 'net/http'
require 'uri'

module GTFS
  module Realtime

    def self.get(url, headers = {})
      uri = URI.parse(url)
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = (uri.scheme == 'https')

      request = Net::HTTP::Get.new(uri.request_uri)
      headers.each do |name, value|
        request[name] = value
      end

      response = http.request(request)

      if response.code == '200'
        GTFS::Realtime::FeedMessage.parse response.body
      else
        response
      end
    end

  end
end

require 'gtfs/realtime/resources'

module GTFS::Realtime::Helpers

  module Message
    def unset?
      fields.all? do |tag, _|
        !value_for_tag?(tag)
      end
    end

    alias blank? unset?
  end

  module FeedMessage
    def trip_updates
      entity.select(&:trip_update?).map(&:trip_update).compact
    end

    def vehicle_positions
      entity.select(&:vehicle?).map(&:vehicle).compact
    end

    def service_alerts
      entity.select(&:alert?).map(&:alert).compact
    end
  end

  module VehiclePosition
    def trip_id
      trip.trip_id
    end

    def route_id
      trip.route_id
    end

    def vehicle_id
      vehicle.id
    end

  end

  module Alert
    def stop_id
      informed_entity.stop_id
    end

    def line_id
      informed_entity.line_id
    end

    def cause_name
      GTFS::Realtime::Alert::Cause.value_to_names_map[cause].first.to_s
    end

    def severity_level_name
      GTFS::Realtime::Alert::SeverityLevel.value_to_names_map[severity_level].first.to_s
    end

    def effect_name
      GTFS::Realtime::Alert::Effect.value_to_names_map[effect].first.to_s
    end

    def header_text_translation
      header_text.translation.each_with_object({}) { |translation, hash| hash[translation.language] = translation.text }
    end

    def description_text_translation
      description_text.translation.each_with_object({}) { |translation, hash| hash[translation.language] = translation.text }
    end
  end

  module FeedEntity

    def trip_update?
      !trip_update&.trip.unset?
    end

    def vehicle?
      !vehicle&.unset?
    end

    def alert?
      !alert&.unset?
    end
  end
end

class ProtocolBuffers::Message
  include GTFS::Realtime::Helpers::Message
end

class GTFS::Realtime::FeedMessage
  include GTFS::Realtime::Helpers::FeedMessage
end

class GTFS::Realtime::FeedEntity
  include GTFS::Realtime::Helpers::FeedEntity
end

class GTFS::Realtime::VehiclePosition
  include GTFS::Realtime::Helpers::VehiclePosition
end

class GTFS::Realtime::Alert
  include GTFS::Realtime::Helpers::Alert
end
